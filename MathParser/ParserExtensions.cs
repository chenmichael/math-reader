﻿using System.Diagnostics;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using MathParser.Expressions;

namespace MathParser;

public partial class MathParser {
    /// <inheritdoc cref="Evaluate(string)"/>
    public static int Evaluate(Stream input) => Parse(input).Evaluate();
    /// <summary>
    /// Parses and evaluates the input value.
    /// </summary>
    /// <param name="input">Input that contains a character stream.</param>
    /// <exception cref="RecognitionException">Could not recognize the input (either a parsing or lexing failed).</exception>
    /// <returns>Evaluated expression represented by the input.</returns>
    public static int Evaluate(string input) => Parse(input).Evaluate();
    public static int Evaluate(CompilationunitContext input) => GetExpression(input).Evaluate();
    public static int Evaluate(ExprContext input) => GetExpression(input).Evaluate();
    /// <inheritdoc cref="Parse(ICharStream)"/>
    public static Expression<int> Parse(Stream input) => Parse(new AntlrInputStream(input));
    /// <inheritdoc cref="Parse(ICharStream)"/>
    public static Expression<int> Parse(string input) => Parse(new AntlrInputStream(input));
    /// <inheritdoc cref="Parse(ITokenSource)"/>
    private static Expression<int> Parse(ICharStream input) => Parse(new MathLexer(input));
    /// <inheritdoc cref="Parse(ITokenStream)"/>
    private static Expression<int> Parse(ITokenSource input) => Parse(new CommonTokenStream(input));
    /// <inheritdoc cref="Parse(MathParser)"/>
    private static Expression<int> Parse(ITokenStream input) => Parse(new MathParser(input) {
        ErrorHandler = new BailErrorStrategy()
    });
    /// <summary>
    /// Parses the input value and returns the represented expression.
    /// </summary>
    /// <param name="input">Input that contains a character stream.</param>
    /// <exception cref="RecognitionException">Could not recognize the input (either a parsing or lexing failed).</exception>
    /// <returns>Expression object represented by the input.</returns>
    private static Expression<int> Parse(MathParser input) {
        try {
            var parsed = input.compilationunit();
            if (parsed.exception is not null and var exception) throw exception;
            return GetExpression(parsed);
        } catch (ParseCanceledException e) {
            if (e.InnerException is RecognitionException re) throw re;
            throw new UnreachableException($"Expected the {nameof(ParseCanceledException)} to have a {nameof(RecognitionException)} inner exception!");
        }
    }
    public static Expression<int> GetExpression(CompilationunitContext input) => GetExpression(input.result);
    public static Expression<int> GetExpression(ExprContext input) => input.result;
}