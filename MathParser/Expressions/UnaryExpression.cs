﻿namespace MathParser.Expressions;

public abstract record class UnaryExpression<T>(Expression<T> Argument) : Expression<T>;