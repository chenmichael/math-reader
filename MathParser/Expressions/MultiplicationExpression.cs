﻿namespace MathParser.Expressions;

public record class MultiplicationExpression<T>(Expression<T> Left, Expression<T> Right) : BinaryExpression<T>(Left, Right) where T : IMultiplyOperators<T, T, T> {
    protected override T Operation(T left, T right) => left * right;
}
