﻿namespace MathParser.Expressions;

public record class SubtractionExpression<T>(Expression<T> Left, Expression<T> Right) : BinaryExpression<T>(Left, Right) where T : ISubtractionOperators<T, T, T> {
    protected override T Operation(T left, T right) => left - right;
}
