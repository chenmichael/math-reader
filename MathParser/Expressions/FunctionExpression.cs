﻿namespace MathParser.Expressions;

public record class FunctionExpression<T>(string Name, Expression<T> Argument) : UnaryExpression<T>(Argument) {
    public override T Evaluate() => throw new NotImplementedException($"Function {Name}!");
}
