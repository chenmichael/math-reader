﻿namespace MathParser.Expressions;

public record class DivisionExpression<T>(Expression<T> Left, Expression<T> Right) : BinaryExpression<T>(Left, Right) where T : IDivisionOperators<T, T, T> {
    protected override T Operation(T left, T right) => left / right;
}
