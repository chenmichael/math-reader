﻿namespace MathParser.Expressions;

public abstract record class BinaryExpression<T>(Expression<T> Left, Expression<T> Right) : Expression<T> {
    public override T Evaluate() => Operation(Left.Evaluate(), Right.Evaluate());
    protected abstract T Operation(T left, T right);
}
