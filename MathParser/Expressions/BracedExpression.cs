﻿namespace MathParser.Expressions;

public record class BracedExpression<T>(Expression<T> Argument) : UnaryExpression<T>(Argument) {
    public override T Evaluate() => Argument.Evaluate();
}
