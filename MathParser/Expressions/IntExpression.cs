﻿namespace MathParser.Expressions;

public record class ImmediateExpression<T>(T Value) : Expression<T> where T : IParsable<T> {
    public static ImmediateExpression<T> Parse(string input) => new(T.Parse(input, null));
    public override T Evaluate() => Value;
}