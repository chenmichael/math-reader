﻿namespace MathParser.Expressions;

public record class AdditionExpression<T>(Expression<T> Left, Expression<T> Right) : BinaryExpression<T>(Left, Right) where T : IAdditionOperators<T, T, T> {
    protected override T Operation(T left, T right) => left + right;
}
