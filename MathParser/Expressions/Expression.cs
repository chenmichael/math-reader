﻿namespace MathParser.Expressions;

public abstract record class Expression<T> {
    public abstract T Evaluate();
}
