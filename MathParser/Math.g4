grammar Math;

@header {
    #nullable disable
    using global::MathParser.Expressions;
}

fragment DIGIT : [0-9] ;
fragment LOWERCASE : [a-z];
PLUS : '+' ;
MINUS : '-' ;
TIMES : '*' ;
DIVIDE : '/' ;
LPAREN : '(' ;
RPAREN : ')' ;
INT : DIGIT+ ;
ID: LOWERCASE+ ;
WS: [ \t\n\r\f]+ -> channel(HIDDEN) ;

compilationunit : result=expr EOF ;

expr returns [Expression<int> result]
    : LPAREN inner=expr RPAREN { $result = new BracedExpression<int>($inner.result); } # Braced
    | name=ID LPAREN argument=expr RPAREN { $result = new FunctionExpression<int>($name.text, $argument.result); } # Function
    | left=expr TIMES right=expr { $result = new MultiplicationExpression<int>($left.result, $right.result); } # Multiplication
    | left=expr DIVIDE right=expr { $result = new DivisionExpression<int>($left.result, $right.result); } # Division
    | left=expr PLUS right=expr { $result = new AdditionExpression<int>($left.result, $right.result); } # Addition
    | left=expr MINUS right=expr { $result = new SubtractionExpression<int>($left.result, $right.result); } # Subtraction
    | value=INT { $result = ImmediateExpression<int>.Parse($value.text); } # Immediate
    ;
