using Antlr4.Runtime;

namespace MathParser.Tests;

[TestClass]
public class MathParserTests {
    [TestMethod]
    public void TestSimpleAddition() {
        var result = MathParser.Evaluate("2+2");
        Assert.AreEqual(4, result);
    }

    [TestMethod]
    public void TestSimpleSubtraction() {
        var result = MathParser.Evaluate("5-3");
        Assert.AreEqual(2, result);
    }

    [TestMethod]
    public void TestSimpleMultiplication() {
        var result = MathParser.Evaluate("2*3");
        Assert.AreEqual(6, result);
    }

    [TestMethod]
    public void TestSimpleDivision() {
        var result = MathParser.Evaluate("6/2");
        Assert.AreEqual(3, result);
    }

    [TestMethod]
    public void TestComplexExpression() {
        var result = MathParser.Evaluate("2+3*4-6/2");
        Assert.AreEqual(11, result);
    }

    [TestMethod]
    public void BasicArithmeticOperations() {
        Assert.AreEqual(MathParser.Evaluate("2 + 2"), 4);
        Assert.AreEqual(MathParser.Evaluate("3 * 4"), 12);
        Assert.AreEqual(MathParser.Evaluate("10 / 2"), 5);
        Assert.AreEqual(MathParser.Evaluate("7 - 2"), 5);
    }

    [TestMethod]
    public void OrderOfOperations() {
        Assert.AreEqual(MathParser.Evaluate("2 + 3 * 4"), 14);
        Assert.AreEqual(MathParser.Evaluate("(2 + 3) * 4"), 20);
        Assert.AreEqual(MathParser.Evaluate("2 / 2 + 3 * 4"), 13);
        Assert.AreEqual(MathParser.Evaluate("2 + 3 * 4 / 2"), 8);
    }

    [TestMethod]
    public void ParenthesesHandling() {
        Assert.AreEqual(MathParser.Evaluate("(2 + 3)"), 5);
        Assert.AreEqual(MathParser.Evaluate("2 * (3 + 4)"), 14);
        Assert.AreEqual(MathParser.Evaluate("(2 + 3) * (4 - 2)"), 10);
        Assert.AreEqual(MathParser.Evaluate("((2 + 3) * 2) / 2"), 5);
    }

    [TestMethod]
    public void InvalidInputHandling() {
        Assert.ThrowsException<NoViableAltException>(() => MathParser.Evaluate("2 + "));
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 +a"));
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 * (3 + 4"));
    }

    [TestMethod]
    public void DivisionByZero() {
        Assert.ThrowsException<DivideByZeroException>(() => MathParser.Evaluate("5 / 0"));
        Assert.ThrowsException<DivideByZeroException>(() => MathParser.Evaluate("(5 + 3) / (2 - 2)"));
    }

    [TestMethod]
    public void UnsupportedOperators() {
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 % 3"));
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 ^ 3"));
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 & 3"));
    }

    [TestMethod]
    public void LargeNumbers() {
        Assert.ThrowsException<OverflowException>(() => MathParser.Evaluate("99999999999999999999999999999 * 99999999999999999999999999999"));
        //Assert.ThrowsException<OverflowException>(() => MathParser.Evaluate("1e+308 * 2"));
    }

    [TestMethod]
    public void UnexpectedCharacters() {
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 + 3 $ 4"));
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 + 3 # 4"));
        Assert.ThrowsException<InputMismatchException>(() => MathParser.Evaluate("2 + 3 @ 4"));
    }
}